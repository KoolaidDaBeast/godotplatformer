extends Node

class_name StateMachine

var states : Array[State]
@export var current_state : State
@export var player : CharacterBody2D

func _ready():
	for child in get_children():
		if (child is State):
			states.append(child)
			child.player = player
		else:
			print("Child " + child.name + " is not a state machine.")
			
func _input(event):
	current_state.input_event(event)
			
func _physics_process(delta):
	current_state.physics_event(delta)
	
	if (current_state.next_state != null):
		current_state = current_state.next_state
		current_state.next_state = null
