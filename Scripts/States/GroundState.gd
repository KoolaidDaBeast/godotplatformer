extends State

@export var JUMP_VELOCITY : float = -150
@export var air_state : State

func physics_event(delta : float):
	if Input.is_action_pressed("jump"):
		player.velocity.y = JUMP_VELOCITY
		next_state = air_state
