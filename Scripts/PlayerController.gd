extends CharacterBody2D

@export var SPEED : float = 150
@onready var sprite : Sprite2D = $Sprite
@onready var animation_tree : AnimationTree = $AnimationTree

var direction : Vector2 = Vector2.ZERO
var last_direction : int = 1

func _ready():
	animation_tree.active = true

func play_animation():
	# Play run animation
	if (direction.x != 0):
		animation_tree.set("parameters/Move/blend_position", direction.x)
		last_direction = direction.x
	else:
		animation_tree.set("parameters/Move/blend_position", direction.x)
		
	# Flip the sprite based on last known direction
	if last_direction >= 0:
		sprite.flip_h = false
	else:
		sprite.flip_h = true

func _physics_process(delta):
	# By default the velocity is zero
	velocity.x = 0
	direction = Input.get_vector("left", "right", "jump", "ui_down").sign()

	if direction.x >= 0:
		velocity.x = direction.x * SPEED
	else:
		velocity.x = direction.x * SPEED

	play_animation()	
	move_and_slide()
