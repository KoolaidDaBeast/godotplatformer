extends State

@export var ground_state : State
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	if not player.is_on_floor():
		player.velocity.y += gravity * delta
	else:
		next_state = ground_state
